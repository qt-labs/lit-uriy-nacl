
#ifndef QNACLUNIMPLEMENTED_H
#define QNACLUNIMPLEMENTED_H

#ifdef Q_OS_NACL

// pthread
#include <pthread.h>
#define PTHREAD_CANCEL_DISABLE 1
#define PTHREAD_CANCEL_ENABLE 2
#define PTHREAD_INHERIT_SCHED 3
void pthread_cleanup_push(void (*handler)(void *), void *arg);
void pthread_cleanup_pop(int execute);

int pthread_setcancelstate(int state, int *oldstate);
int pthread_setcanceltype(int type, int *oldtype);
void pthread_testcancel(void);
int pthread_cancel(pthread_t thread);

int pthread_attr_setinheritsched(pthread_attr_t *attr,
    int inheritsched);
int pthread_attr_getinheritsched(const pthread_attr_t *attr, 
    int *inheritsched);

// event dispatcher, select
//struct fd_set;
//struct timeval;
int fcntl(int fildes, int cmd, ...);
int select(int nfds, fd_set * readfds, fd_set * writefds, fd_set * errorfds, struct timeval * timeout);
int sigaction(int sig, const struct sigaction * act, struct sigaction * oact);

int open(const char *pathname, int oflag, ...);
int open64(const char *pathname, int oflag, ...);

#endif //Q_OS_NACL

#endif //QNACLUNIMPLEMENTED_H 
