/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the QtGui module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** No Commercial Usage
** This file contains pre-release code and may not be distributed.
** You may use this file in accordance with the terms and conditions
** contained in the either Technology Preview License Agreement or the
** Beta Release License Agreement.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain
** additional rights. These rights are described in the Nokia Qt LGPL
** Exception version 1.0, included in the file LGPL_EXCEPTION.txt in this
** package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you are unsure which license is appropriate for your use, please
** contact the sales department at http://qt.nokia.com/contact.
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qeventdispatcher_nacl_p.h"

#include "qplatformdefs.h"
#include "qapplication.h"
#include "qwaitcondition.h"
#include "qevent.h"
#include "qmutex.h"
#include "qobject.h"
#include <errno.h>
#include <stdio.h>

#include <private/qapplication_p.h>
#include <nacl/nacl_av.h>
#include <time.h>

QT_BEGIN_NAMESPACE

QT_USE_NAMESPACE

Qt::MouseButtons naClButtonStateToQtMouseButton(uint8_t &state)
{
    Qt::MouseButtons buttons;
    if (state & NACL_MOUSE_STATE_LEFT_BUTTON_PRESSED)
        buttons |= Qt::LeftButton;
    if (state & NACL_MOUSE_STATE_MIDDLE_BUTTON_PRESSED)
        buttons |= Qt::MidButton;
    if (state & NACL_MOUSE_STATE_RIGHT_BUTTON_PRESSED)
        buttons |= Qt::RightButton;
    return buttons;
}

class NaClTimerInfo
{
public:
    QObject *object;
    int id;
    int interval;
    int elapsed;
};

class QEventDispatcherNaClPrivate : public QEventDispatcherUNIXPrivate
{
    Q_DECLARE_PUBLIC(QEventDispatcherNaCl)
public:
    inline QEventDispatcherNaClPrivate()
    { }
//    QList<NaClEvent*> queuedUserInputEvents;
    QWaitCondition delay;
    QMutex mutex;
    QList<NaClTimerInfo> timers;
};


QEventDispatcherNaCl::QEventDispatcherNaCl(QObject *parent)
    : QEventDispatcherUNIX(*new QEventDispatcherNaClPrivate, parent)
{ }

QEventDispatcherNaCl::~QEventDispatcherNaCl()
{ }


bool QEventDispatcherNaCl::processEvents(QEventLoop::ProcessEventsFlags flags)
{
    do {
        QApplication::sendPostedEvents();
        NaClMultimediaEvent event;
        if (nacl_video_poll_event(&event) != 0) {
            activateTimers();
            continue;
        }

        if (event.type == NACL_EVENT_QUIT) {
            perror(" quit");
            qApp->quit();
            break;
        } else {
            translateAndSendEvent(event);        
        }
        
        activateTimers();
    } while(flags & QEventLoop::WaitForMoreEvents); // busy-wait for more events

    return false;
}

void QEventDispatcherNaCl::translateAndSendEvent(NaClMultimediaEvent event)
{
    if (event.type == NACL_EVENT_MOUSE_BUTTON_DOWN) {
        //perror("mouse down");
        QPoint p(event.button.x, event.button.y);
        
        //fprintf(stderr, "mouse coords %d %d \n", event.button.x, event.button.y);
        
        QMouseEvent mouseEvent(QEvent::MouseButtonPress, p, p, Qt::LeftButton,
                               Qt::LeftButton, Qt::NoModifier);
        QApplicationPrivate::handleMouseEvent(0, mouseEvent);
        
        //qApp->handleMouseEvent(0, &mouseEvent);
        //event
    }
    
    if (event.type == NACL_EVENT_MOUSE_BUTTON_UP) {
        //perror("mouse up");
        QPoint p(event.button.x, event.button.y);
        QMouseEvent mouseEvent(QEvent::MouseButtonRelease, p, p, Qt::LeftButton,
                               Qt::NoButton, Qt::NoModifier);
        QApplicationPrivate::handleMouseEvent(0, mouseEvent);
    }
    
    if (event.type == NACL_EVENT_MOUSE_MOTION) {
        //fprintf(stderr, " mouse motion %d %d", event.motion.x, event.motion.y);
        QPoint p(event.motion.x, event.motion.y);
        QMouseEvent mouseEvent(QEvent::MouseMove, p, p,
                               Qt::NoButton,
                               naClButtonStateToQtMouseButton(event.motion.state),
                               Qt::NoModifier);
        QApplicationPrivate::handleMouseEvent(0, mouseEvent);
    }
    
    if (event.type == NACL_EVENT_KEY_DOWN) {
        //fprintf(stderr, "unicode %d", event.key.keysym.unicode);
        QKeyEvent keyEvent(QEvent::KeyPress, Qt::Key_A, Qt::NoModifier,
                           QString(QChar(event.key.keysym.sym)));
        QApplicationPrivate::handleKeyEvent(0, &keyEvent);
        
    }
    
    if (event.type == NACL_EVENT_KEY_UP) {
        QKeyEvent keyEvent(QEvent::KeyRelease, Qt::Key_A, Qt::NoModifier,
                           QString(QChar(event.key.keysym.sym)));
        QApplicationPrivate::handleKeyEvent(0, &keyEvent);
    }
}


bool QEventDispatcherNaCl::hasPendingEvents()
{
    perror("QEventDispatcherNaCl::hasPendingEvents()");
    extern uint qGlobalPostedEventsCount(); // from qapplication.cpp
    return qGlobalPostedEventsCount();
}

void QEventDispatcherNaCl::registerSocketNotifier(QSocketNotifier *notifier)
{
    perror("QEventDispatcherNaCl::registerSocketNotifier()");
}

void QEventDispatcherNaCl::unregisterSocketNotifier(QSocketNotifier *notifier)
{
    perror("QEventDispatcherNaCl::unregisterSocketNotifier()");
}

void QEventDispatcherNaCl::startingUp()
{



}

void QEventDispatcherNaCl::closingDown()
{

}

void QEventDispatcherNaCl::wakeUp()
{
 //   perror("QEventDispatcherNaCl::wakeUp()");
}

void QEventDispatcherNaCl::interrupt()
{
    perror("QEventDispatcherNaCl::interrupt()");
}

void QEventDispatcherNaCl::flush()
{
    perror("QEventDispatcherNaCl::flush()");
    if(qApp)
        qApp->sendPostedEvents();
}

QT_END_NAMESPACE
