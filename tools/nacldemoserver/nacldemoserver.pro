# -------------------------------------------------
# Project created by QtCreator 2009-11-17T06:06:52
# -------------------------------------------------
QT += network
TARGET = nacldemoserver
TEMPLATE = app
SOURCES += main.cpp \
    serverwidget.cpp \
    httpserver.cpp
HEADERS += serverwidget.h \
    httpserver.h
FORMS += serverwidget.ui
RESOURCES += nacldemoserver.qrc
